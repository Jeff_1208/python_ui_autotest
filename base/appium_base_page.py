from time import sleep

from appium.webdriver.mobilecommand import MobileCommand
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from base_page import BasePage
from utils.get_logger import GetLogger


class AppiumBasePage(BasePage):

    def __init__(self, driver):
        self.logger = GetLogger().get_logger()
        self.driver = driver

    # 屏幕向上滑动
    def swipe_up(self, duration=1000):  # duration 滑动时间（默认5毫秒）；
        l = self.getSize()
        x1 = int(l[0] * 0.5)  # x坐标
        y1 = int(l[1] * 0.75)  # 起始y坐标
        y2 = int(l[1] * 0.25)  # 终点y坐标
        self.driver.swipe(x1, y1, x1, y2, duration)

    def swipe_with_location(self,start_x_per, end_x_per, start_y_per, end_y_per, duration=1000):
        """

        :param start_x_per: 0~1
        :param end_x_per: 0~1
        :param driver: 0~1
        :param start_y_per: 0~1
        :param end_y_per: 0~1
        :param duration: 毫秒为单位
        :return:
        """
        l = self.get_size()
        x1 = int(l[0] * start_x_per)  # x坐标
        x2 = int(l[0] * end_x_per)      # 终点x坐标
        y1 = int(l[1] * start_y_per)  # 起始y坐标
        y2 = int(l[1] * end_y_per)  # 终点y坐标
        self.driver.swipe(x1, y1, x1, y2, duration)

    def scroll_up_to_find_element(self,loc,max_times,timeout=3,poll=0.5):
        count = 0
        ele = None
        while True:
            if ele is not None:
                break;
            if count == max_times:
                self.logger .error("{0}页面中未能找到{1}元素".format(self, loc))
                raise Exception
            try:
                # ele = WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_element(*loc))   #这种方式会fail
                ele = WebDriverWait(self.driver, timeout, poll).until(EC.visibility_of(self.driver.find_element(*loc)))
                self.logger .info("{0}页面中找到{1}元素".format(self, loc))
            except Exception as msg:
                self.logger .error("{0}页面中未能找到{1}元素".format(self, loc))
                self.swipe_up()
                sleep(0.2)
                count += 1

    def open_app(self,package_name):
        self.driver.activate_app(package_name)

    def background_app(self,seconds=5):
        self.driver.background_app(seconds)

    def open_notifications(self):
        self.driver.open_notifications()

    def start_activity(self, app_package: str, app_activity: str):
        self.driver.start_activity(app_package,app_activity)

    def install_app(self, app_path: str):
        self.driver.install_app(app_path)

    def close_app(self):
        self.driver.close_app()

    def hide_keyboard(self):
        self.driver.hide_keyboard()
