# -*- coding:utf-8 -*-
# 功能：基类，对 Selenium api 进行二次封装
import glob
import os
from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

from utils.calculate_time_difference import do_time
from utils.datetime_util import DateTimeUtil
from utils.get_logger import GetLogger
from utils.get_path_info import GetPathInfo
from utils.get_yml_data import GetYmlData
from time import sleep


class BasePage(object):
    def __init__(self, driver):
        """
        实例化BasePage类时，最先执行的是__init__()方法，该方法的入参就是BasePage类的入参
        :param driver:
        """
        self.logger = GetLogger().get_logger()
        self.yml = GetYmlData()
        self.time = DateTimeUtil()

        # try:
        #     self.driver = webdriver.Chrome()
        # except Exception:
        #     raise NameError("No Chrome.")

        self.driver = driver

    def wait_ele_visible_(self, loc, doc, times=3, poll_frequency=0.5):
        """
        等待元素可见
        :param loc: 元素定位
        :param doc: 元素描述
        :param times: 最长等待时间
        :param poll_frequency: 轮询频率，调用 until 或 until_not方法中的间隔时间，默认为0.5秒
        :return:
        """
        try:
            @do_time
            def fun():
                WebDriverWait(self.driver, times, poll_frequency).until(EC.visibility_of_element_located(loc))

            self.logger.info("等待【{}】元素【{}】出现，耗时：{}豪秒".format(doc, loc, fun()))
        except Exception:
            self.logger.error("等待【{}】元素【{}】出现失败".format(doc, loc))
            self.save_screenshot_(doc)
            raise

    def find_element_(self, loc, doc):
        """
        查找元素
        :param loc: 元素定位 例如：login_icon_loc = (By.ID, "sb_form_q")
        :param doc:
        :return:
        """
        try:
            self.logger.info("开始查找【{}】元素【{}】".format(doc, loc))
            self.wait_ele_visible_(loc, doc)
            return self.driver.find_element(*loc)
        except Exception:
            self.logger.error("查找【{}】元素【{}】失败".format(doc, loc))
            self.save_screenshot_(doc)
            raise

    def _click_button(self, loc, doc, times=3, poll_frequency=0.5):
        """
        点击元素
        :param loc: 元素定位
        :param doc: 元素描述
        :param times: 最长等待时间
        :param poll_frequency: 轮询时间
        :return:
        """
        try:
            self.wait_ele_visible_(loc, doc, times, poll_frequency)
            self.find_element_(loc, doc).click()
            self.logger.info("点击【{}】元素【{}】成功".format(doc, loc))
        except Exception:
            self.logger.error("点击【{}】元素【{}】失败".format(doc, loc))
            self.save_screenshot_(doc)
            raise

    def _send_keys(self, loc, text, doc, timeout=3, frequency=0.5, clear=True):
        """
        输入文本
        :param loc: 元素位置
        :param text: 要输入的文本内容
        :param doc: 元素描述
        :param timeout: 超时时间
        :param frequency: 频率
        :param clear: 是否清空文本框内容，默认为True
        :return:
        """
        try:
            self.logger.info("开始在【{}】元素中输入文本【{}】".format(doc, text))
            self.wait_ele_visible_(loc, doc, timeout, frequency)
            # time.sleep(10)
            if clear:
                self.find_element_(loc, doc).clear()
            self.find_element_(loc, doc).send_keys(text)
        except Exception:
            self.logger.exception("在【{}】元素中输入文本【{}】失败".format(doc, text))
            self.save_screenshot_(doc)
            raise

    def save_screenshot_(self, pic_name):
        """
        保存截图
        :param pic_name: 截图图片名称
        :return: None
        """
        try:
            # 图片命名：元素名称-年-月-日-时-分-秒.png
            screenshot_path = GetPathInfo().get_project_path() + r"/reports/screenshot"
            if not os.path.exists(screenshot_path):
                os.makedirs(screenshot_path)
            file_name = screenshot_path + "/{}_{}.png".format(self.time.get_now_datetime_v2(), pic_name)
            self.driver.get_screenshot_as_file(file_name)
            self.logger.info("成功截图保存到{}".format(file_name))
        except Exception:
            self.logger.error("截图失败")
            raise

    def script(self, src):
        """
        执行JavaScript脚本
        :param src: JavaScript脚本
        """
        self.driver.execute_script(src)

    def _get_text(self, loc, doc):
        """
        获取文本信息
        :param loc:
        :param doc:
        :return: 文本信息
        """
        try:
            return self.find_element_(loc, doc).text
        except Exception:
            self.logger.error("获取文本失败")
            raise

    def _move_to_element(self, loc, doc):
        """
        鼠标悬浮
        :param loc: 元素定位
        :param doc: 描述
        :return:
        """
        try:
            self.logger.info("鼠标开始悬停在【{}】元素上".format(loc))
            ele = self.find_element_(loc, doc)
            ActionChains(self.driver).move_to_element(ele).perform()
        except Exception:
            self.logger.error("在元素【{}】进行悬浮失败！".format(loc))
            self.save_screenshot_(doc)
            raise

    # start self function
    def click_element(self, ele: WebElement, sleep_time=0.2):
        try:
            ele.click()
            # self.logger.info("Click element {}".format(ele))
            # self.logger.info("{0}页面中找到{1}元素".format(self, ele))
            self.logger.info("等待【{}】元素【】出现，耗时：豪秒".format(ele))
            sleep(sleep_time)
        except Exception as e:
            print('except:', e)
            sleep(2)
            self.logger.error("Failed to Click element {}".format(ele))
            sleep(5)
            raise e

    def input_ele_by_ele(self, ele: WebElement, text):
        ele.clear()
        ele.send_keys(text)

    def is_ele_visible(self, ele: WebElement, timeout=30):
        try:
            self.wait_until_element_visible(ele, timeout)
            return True
        except Exception as e:
            print('except:', e)
            return False

    def open_url(self, url: str) -> None:
        self.driver.get(url)

    def switch_to_last_page(self):
        windows = self.driver.window_handles
        self.driver.switch_to.window(windows[-1])

    def get_alert(self):
        try:
            alert = self.driver.switch_to.alert()
            print(alert.text)
            alert.accept()
        except Exception as msg:
            self.logger.error("{0}页面中未能找到alert".format(self))

    def find_ele_for_exception(self, loc, timeout=20, poll=0.5):
        return WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_element(*loc))

    def find_elements(self, loc, timeout=20, poll=0.5):
        return WebDriverWait(self.driver, timeout, poll).until(lambda x: x.find_elements(*loc))

    # <option value="005">Credit Agricole Corporate and Investment Bank (005)</option>
    # input元素中的值需要通过这种方式来获取，ele.text不能获取input的值
    def get_ele_attribute_value(self,loc,attribute_value):  #获取元素中熟悉value的值 005
        return self.find_ele(loc).get_attribute(attribute_value).strip()

    def get_ele_attribute_text(self,loc):   #获取元素在浏览器显示的值 Credit Agricole Corporate and Investment Bank (005)
        return self.find_ele(loc).text.strip()

    def scroll_windows(self,js):
        self.driver.execute_script(js)

    def execute_js(self,js,ele):
        self.driver.execute_script(js, ele)   #js删除某个元素的属性,所以需要元素作为参数

    def switch_windows(self):
        current_window = self.driver.current_window_handle
        windows = self.driver.window_handles
        for i in windows:
            if current_window != i:
                self.driver.switch_to.window(i)

    def close_and_switch_window(self,first_window):
        self.driver.close()
        self.driver.switch_to.window(first_window)

    def switch_last_window(self):
        windows = self.driver.window_handles
        self.driver.switch_to.window(windows[-1])
        self.driver.maximize_window()
        print("Switch to the latest window and wait")

    def select_a_ele_from_select(self, loc, value,sleep_time=0.1):
        ele=self.find_ele(loc)
        ele.click()
        sleep(sleep_time)
        Select(ele).select_by_visible_text(value)

    # <option value="005">Credit Agricole Corporate and Investment Bank (005)</option>
    def select_a_ele_from_select_by_flag(self, loc, flag, value,sleep_time=0.1):
        ele = self.find_ele( loc)
        ele.click()
        sleep(sleep_time)
        if "value" in flag:   #获取元素中熟悉value的值 005
            Select(ele).select_by_value(value)
        elif "text" in flag:  #获取元素在页面显示的值
            Select(ele).select_by_visible_text(value)
        elif "id" in flag:
            Select(ele).select_by_index(value)

    def picturereading(self,path):    #bookneme输入图片名字方便在图片目录中找到对应的图片
        # image = glob.glob(path + '*.png')
        image = glob.glob(path)
        return image

    def switch_frame(self, loc):
        """
        多表单嵌套切换
        :param loc: 传元素的属性值
        :return: 定位到的元素
        """
        try:
            return self.driver.switch_to_frame(loc)
        except Exception as msg:
            self.logger.error("查找iframe异常-> {0}".format(msg))

    def switch_alert(self):
        """
        警告框处理
        :return:
        """
        try:
            return self.driver.switch_to_alert()
        except Exception as msg:
            self.logger.error("查找alert弹出框异常-> {0}".format(msg))

    def close_page(self):
        self.driver.close()

    def close_browser(self) -> None:
        self.driver.quit()

    def implicitly_wait(self,timeout=10):
        self.driver.implicitly_wait(timeout)

    def execute_script(self, script):
        self.driver.execute_script(script)

    def scroll_window_top(self):
        js = "var q=document.documentElement.scrollTop=0"
        self.driver.execute_script(js)  # 将滚动条移动到页面的顶部
        sleep(1)