import os
import shutil


def check_create_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


def copy_file(source_path, target_path):
    shutil.copy(source_path, target_path)


def copy_folder(source_folder, target_folder):
    print("Copy screenshot from " + source_folder + " to " + target_folder + "!!!")
    check_create_path(target_folder)
    for ipath in os.listdir(source_folder):
        fulldir = os.path.join(source_folder, ipath)  # 拼接成绝对路径
        # print(fulldir)         #打印相关后缀的文件路径及名称
        if os.path.isfile(fulldir):  # 文件，匹配->打印
            shutil.copy(fulldir, target_folder)


# 如果需要copy文件夹中子文件夹，请使用一下方法，递归方式复制
new_path = r'F:\2'  # 新路径


def copy_folder_with_subfolder(path):
    for ipath in os.listdir(path):
        fulldir = os.path.join(path, ipath)  # 拼接成绝对路径
        print(fulldir)  # 打印相关后缀的文件路径及名称
        if os.path.isfile(fulldir):  # 文件，匹配->打印
            shutil.copy(fulldir, new_path)
        if os.path.isdir(fulldir):  # 目录，递归
            copy_folder_with_subfolder(fulldir)
