import os
import shutil
import time
from utils.copy_file import copy_folder
from utils.Global import SCREENSHOT_DIR
from utils.common import check_path


class SutieHook():

    report_path = "./reports"
    log_path = "./target/logs"

    def get_path(self):
        return os.getcwd()

    def start_module(self):
        """    整个.模块只在开始时执行一次    """
        self.remove_report_folder(20)
        self.remove_json_file()
        self.remove_log_files(5)
        self.remove_files(SCREENSHOT_DIR)

    def end_module(self):
        """    整个.模块只在结束时执行一次    """
        time.sleep(2)
        now_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime(time.time()))
        report_folder = "report_" + now_str
        time.sleep(5)
        # os.system("cd C:/Users/jeff.xie/PycharmProjects/MyProject/po_frame/report1")
        print("report_folder:"+report_folder)
        print("KKKKKKKKKKKKKKKKKKKKKKKKKKKK")
        print(self.get_path())
        print("KKKKKKKKKKKKKKKKKKKKKKKKKKKK")
        os.chdir(self.report_path)
        # os.chdir(r"./report1")
        print(self.get_path())
        os.mkdir(report_folder)
        time.sleep(0.8)
        os.chdir(r"..")
        print("report_folder: ", report_folder)
        print(self.get_path())
        # os.system("allure generate ./report1 -o ./report1/html/ --clean")
        # os.system("allure generate ./report1/ -o ./report1/{}/html/ --clean".format(report_folder))
        cmd = "allure generate {} -o {}/{}/html/ --clean".format(self.report_path, self.report_path, report_folder)
        print("cmd:" + cmd)
        os.popen(cmd)  # 成功

        print("**********************Check Report*********************")
        print("report_folder:" + report_folder)
        print("**********************Check Report*********************")

        report_screenshot_path = ".\\reports\\"+report_folder+"\\screenshot"
        check_path(report_screenshot_path)
        time.sleep(2)
        copy_folder(SCREENSHOT_DIR, report_screenshot_path)

        return report_folder

    def remove_json_file(self):
        if not os.path.exists(self.report_path):
            return
        os.chdir(self.report_path)
        fs = os.listdir("../")
        for f in fs:
            if os.path.isfile(f):
                os.remove(f)
        os.chdir(r"../..")

    def remove_files(self, path):
        if not os.path.exists(path):
            return
        if os.path.exists(path):
            files = os.listdir(path)
            for f in files:
                file = os.path.join(path,f)
                os.remove(file)

    def remove_report_folder(self, report_num=5):
        if not os.path.exists(self.report_path):
            return
        os.chdir(self.report_path)
        fs = os.listdir("../")
        folder_list = []
        for f in fs:
            if os.path.isdir(f):
                folder_list.append(f)
        if len(folder_list) > report_num:
            for i in range(len(folder_list) - report_num):
                print("remove ./{}".format(folder_list[i]))
                shutil.rmtree(r"./{}".format(folder_list[i]))
        os.chdir(r"../..")

    def remove_log_files(self, log_num=5):
        if not os.path.exists(self.log_path):
            return
        print("os.getcwd(): "+os.getcwd())
        os.chdir(self.log_path)
        fs = os.listdir("../")
        logs_file_list = []
        for f in fs:
            if os.path.isfile(f):
                logs_file_list.append(f)
        delete_file=[]
        if len(logs_file_list) > log_num:
            delete_file = logs_file_list[0:len(logs_file_list) - log_num]

        if delete_file and len(delete_file) > 0:
            for f in delete_file:
                if os.path.isfile(f):
                    print("remove ./{}".format(f))
                    os.remove(f)
        os.chdir(r"../..")
        os.chdir(r"../..")  # 最开始进了两层路径，所以返回两层路径
