# -*- coding:utf-8 -*-
# 作者：测试蔡坨坨(caituotuo.top)
# 时间：2022/6/11 22:12
# 功能：
import time
import os
import allure
import pytest
from selenium import webdriver

from pages.login_page import LoginPage
from utils.get_logger import GetLogger
from utils.get_yml_data import GetYmlData
from utils.suite_hook import SutieHook

yml = GetYmlData()
config = yml.get_yml_data("config/config.yml")
logger = GetLogger.get_logger()
suitehook = SutieHook()
# 运行测试前准备工作
def pytest_configure():
    suitehook.start_module()
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    logger.info("$$$$$$$$$$$$$$setup_module$$$$$$$$$$$$$$$")
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")


# 使用钩子函数执行官setup teardown
# https://blog.csdn.net/panc_guizaijianchi/article/details/126701234
# 运行测试后代码运行
@pytest.hookimpl(tryfirst=True)
def pytest_sessionfinish():
    report_folder = suitehook.end_module()
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    logger.info("$$$$$$$$$$$$$teardown_module$$$$$$$$$$$$$")
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")


global driver
@pytest.fixture(scope="function")
def access_web():
    """
    访问目标网址
    :return:
    """
    global driver
    driver = None  # 需要给一个初始化的值
    driver_type = config["driver_type"]
    url = config["login_url"]
    # 前置：打开浏览器
    try:
        driver = getattr(webdriver, driver_type)()
    except Exception as e:
        logger.error(e)
        driver = webdriver.Chrome()
    driver.get(url)
    driver.maximize_window()
    yield driver
    # 后置：关闭浏览器
    logger.info("----------所有用例执行完毕----------")

    # teardown 添加图片成功
    time.sleep(3)
    fail_picture_path=r"./target/screenshot/conftest_fail.png"
    if os.path.exists(fail_picture_path):
        time.sleep(0.2)
        allure.attach.file(fail_picture_path, "fail_picture",
                           attachment_type=allure.attachment_type.PNG)
    driver.quit()
    return driver


import logging
import os

# 定义日志输出目录
log_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "logs")

# 设置logging模块
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.FileHandler(os.path.join(log_path, 'output.log')),
        logging.StreamHandler()
    ]
)


@pytest.fixture(scope="function")
def login(access_web):
    """
    登录系统
    :param access_web:
    :return:
    """
    data = yml.get_yml_data("data/login/r0.yml")
    LoginPage(access_web).login(data["username"], data["password"])
    yield access_web
    logger.info("----------单条用例执行完毕----------")


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    logger.info("$$$$$pytest_runtest_makereport start$$$$$")
    logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    # 获取钩子方法的调用结果
    out = yield
    # print('用例执行结果', out)


    # 3. 从钩子方法的调用结果中获取测试报告
    report = out.get_result()
    if report.when == "call":
        print('测试报告：%s' % report)
        print('步骤：%s' % report.when)
        print('nodeid：%s' % report.nodeid)
        print('description:%s' % str(item.function.__doc__))
        print(('运行结果: %s' % report.outcome))
        logger.info('nodeid：%s' % report.nodeid)
        logger.info('运行结果: %s' % report.outcome)
        allure.attach("描述", "teardown for case...")

        if report.outcome == "passed":
            print("************PASS***************")
        elif report.outcome in ['failed', 'error']:
            # fail_picture = driver.get_screenshot_as_png()
            picture_name = report.nodeid+"_fail.png"
            picture_name = picture_name.replace("/", "_")
            picture_name = picture_name.replace(":", "_")
            driver.get_screenshot_as_file(r"./target/screenshot/conftest_fail.png")
            driver.get_screenshot_as_file(r"./target/screenshot/" + picture_name)
            time.sleep(0.2)
            # allure.attach.file(r"./target/screenshot/" + picture_name, picture_name, attachment_type=allure.attachment_type.PNG)
    if report.when == "teardown":
        logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        logger.info("$$$$$conftest teardown$$$$$")
        logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        print('测试报告：%s' % report)
        print('步骤：%s' % report.when)
        print('nodeid：%s' % report.nodeid)
        print('description:%s' % str(item.function.__doc__))
        print(('运行结果: %s' % report.outcome))
        logger.info('nodeid：%s' % report.nodeid)
        logger.info('运行结果: %s' % report.outcome)
        allure.attach("描述", "teardown for case...")

        if report.outcome in ['passed']:
            print(('运行结果PASS!!!!!!!!!!!!!!!!'))
        if report.outcome in ['failed', 'error']:
            print(('运行结果FAIL!!!!!!!!!!!!!!!!'))
            # fail_picture = driver.get_screenshot_as_png()
            picture_name = report.nodeid+"_fail.png"
            picture_name = picture_name.replace("/", "_")
            picture_name = picture_name.replace(":", "_")
            driver.get_screenshot_as_file(r"./target/screenshot/conftest_fail.png")
            driver.get_screenshot_as_file(r"./target/screenshot/" + picture_name)
            time.sleep(0.2)
            allure.attach.file(r"./target/screenshot/" + picture_name, picture_name, attachment_type=allure.attachment_type.PNG)


# pytest_runtest_makereport 的用法
# https://blog.csdn.net/mashang_z111/article/details/127395019
"""
装饰器 pytest.hookimpl(hookwrapper=True, tryfirst=True) 解释：
@pytest.hookimpl(hookwrapper=True)装饰的钩子函数，有以下两个作用：
1、可以获取到测试用例不同执行阶段的结果（setup，call，teardown）
2、可以获取钩子方法 pytest_runtest_makereport(item, call) 的调用结果（yield返回一个测试用例执行后的result对象）和调用结果result对象中的测试报告（返回一个report对象）
pytest_runtest_makereport(item, call) 钩子函数参数解释：
1、 item 是测试用例对象；
2、 call 是测试用例的测试步骤；具体执行过程如下：
①先执行 when="setup" ，返回setup用例前置操作函数的执行结果。
②然后执行 when="call" ，返回call测试用例的执行结果。
③最后执行 when="teardown" ，返回teardown用例后置操作函数的执行结果。
结果分析：
从结果可以看到，测试用例的执行过程会经历3个阶段：
setup -> call -> teardown
每个阶段会返回 Result 对象和 TestReport 对象，以及对象属性。（setup和teardown上面的用例默认没有，结果都是passed。）
"""

# fixture() 函数的 setup 前置函数在执行时异常，即 setup 执行结果为 failed ，则后面的 call 测试用例与 teardown 后置操作函数都不会执行。
# 此时的状态是 error ，也就是代表测试用例还没开始执行就已经异常了。（在执行前置操作函数的时候就已经发生异常）

# setup 前置操作函数正常执行，测试用例 call 正常执行， teardown 后置操作函数执行时发生异常。
# 测试用例正常执行，但是测试结果中会有 error ，因为 teardown 后置操作函数在执行时发生异常

# 场景：编写测试用例时，在保证 setup 前置操作函数和 teardown 后置操作函数不报错的前提下，我们一般只需要关注测试用例的执行结果，即只需要获取测试用例执行call的结果。
# 解决办法：因为前面的 pytest_runtest_makereport 钩子方法执行了三次。所以在打印测试报告的相关数据之气可以加个判断： if report.when == "call" 。

# 从0到1精通自动化测试，pytest自动化测试框架，Fixture之conftest.py与yield实现teardown（四）
# https://blog.csdn.net/x2waiwai/article/details/123112168