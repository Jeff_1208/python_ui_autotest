__author__ = 'Jeff.xie'

import logging

import pytest
import allure
from pages.baidu import Baidu
from time import sleep


# cannot collect test class 'Test_Baidu' because it has a __init__ constructor
# pytest 中不能有init方法，所以不能继承WebBasePage类，只能在page中去继承WebBasePage
class Test_Baidu():
    status ="Pending Approval"

    @classmethod
    def teardown_class(self):
        print("Finish TEST！！！！！！！")

    @pytest.mark.skipif("Pending Approval" == status, reason="满足条件，跳过")
    def test_5(self):
        allure.attach('在fixture前置操作里面添加一个附件txt: '+"test5", 'fixture前置附件', allure.attachment_type.TEXT)
        print("555555")

    def test_2(self):
        allure.attach('在fixture前置操作里面添加一个附件txt: '+"test5", 'fixture前置附件', allure.attachment_type.TEXT)
        print("测试输出2222222")

    def test_1(self, access_web):
        logging.info('这是一条测试日志')
        print("测试输出！！！！！！！！！！！")
        self.baidu = Baidu(access_web)
        self.baidu.search_item("python")
        sleep(5)

    # #这里是一个测试用例
    # @pytest.mark.usefixture()   #每一次执行这个方法前，都会去执行get_data方法
    # def test_search(self,get_data):
    #     for i in get_data:
    #         self.baidu.search_item(i)




