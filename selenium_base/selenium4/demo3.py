# function: 打开新的标签页或窗口

import time

from selenium import webdriver

driver = webdriver.Chrome()
driver.get("https://www.baidu.com/")

# 打开新的标签页，并切换进入
driver.switch_to.new_window("tab")
driver.get("https://www.cnblogs.com")
print(driver.title)

# 打开新的窗口，并切换进入
driver.switch_to.new_window("window")
driver.get("https://www.baidu.com/")
print(driver.title)

time.sleep(3)
driver.quit()
