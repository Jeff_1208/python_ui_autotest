__author__ = 'Jeff.xie'

from time import sleep
from typing import Any

from pythium import find_by, find_all, by
from selenium.webdriver.remote.webelement import WebElement
from pythium import Page
from base.base_page import BasePage


class Baidu(BasePage):

    def __init__(self, driver):
        super(Baidu, self).__init__(driver)
        Page.__init__(self, driver)

    # 多个元素定位参考：
    # https: // blog.csdn.net / lb245557472 / article / details / 127884668
    @property
    @find_by(xpath='//*[@id="kw"]')
    def baidu_input(self) -> WebElement: ...

    @property
    @find_by(xpath='//*[@id="su"]')
    def baidu_btn(self) -> WebElement: ...

    @property
    @find_all(by(css=".icon-logo1"), by(css=".icon-logo"))
    def find_all_web_test(self) -> WebElement: return Any

    def search_item(self, item):
        self.input_ele_by_ele(self.baidu_input, item)
        self.click_element(self.baidu_btn)
        sleep(2)
