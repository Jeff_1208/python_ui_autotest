__author__ = 'Jeff.xie'

from time import sleep

from pythium import find_by
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from pythium import Page
from base.base_page import BasePage


class CSDN(Page, BasePage):

    def __init__(self,driver):
        super(CSDN, self).__init__(driver)
        Page.__init__(self, driver)

    def open_url(self,url="https://blog.csdn.net"):
        self.driver.get(url)
        sleep(5)

    @property
    @find_by(class_name='toolbar-btn-loginfun')
    def csdn_login(self) -> WebElement: ...

    @property
    @find_by(xpath='//*[@id="passportbox"]/span')
    def close_item(self) -> WebElement: ...

    @property
    @find_by(xpath='//*[@id="testtest111"]/span')
    def test_item(self) -> WebElement: ...

    def login(self):
        self.click_element(self.csdn_login)
        self.get_screenshot("login")
        sleep(2)
        if self.is_ele_visible(self.close_item, 3):
            self.click_element(self.close_item)
        sleep(2)
        if self.is_ele_visible(self.test_item, 3):
            self.click_element(self.test_item)
        sleep(1)
        self.get_screenshot("last_page")

    # selenium 新的定位元素的方法
    @find_by(xpath='//*[@id="search"]/form/div[3]/input')
    def search_button_element(self) -> WebElement:
        pass






